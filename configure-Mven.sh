#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>private-repo</id>\
  <username>state.of.colorado.nexus</username>\
  <password>syZyEW6dbvND8UPc</password>\
</server>" C:\Users\Rakesh Gupta\.m2\settings.xml

sed -i "/<profiles>/ a\
<profile>\
  <id>private-repo</id>\
  <activation>\
    <activeByDefault>true</activeByDefault>\
  </activation>\
  <repositories>\
    <repository>\
      <id>private-repo</id>\
      <url>http://repo1.maven.org/maven2/</url>\
    </repository>\
  </repositories>\
  <pluginRepositories>\
    <pluginRepository>\
      <id>private-repo</id>\
      <url>https://example.com/path/to/maven/repo/</url>\
    </pluginRepository>\
  </pluginRepositories>\
</profile>" C:\Users\Rakesh Gupta\.m2\settings.xml